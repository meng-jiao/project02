<%@page import="com.situ.entity.User"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	isELIgnored="false" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link href="/plugins/layui/css/layui.css" rel="stylesheet"
	type="text/css" />
<script type="text/javascript" src="/js/jquery-3.6.0.js"></script>
<style type="text/css">
#pic {
	width: 100px;
	height: 100px;
	border-radius: 50%;
	margin: 20px auto;
	cursor: pointer;
}
</style>
</head>
<body>
	<!-- 
		action:   /**(/user/add)   表示请求地址是　　http://ip:端口号/action (http://localhost:8080/user/add　少项目名)
		
		如果　不写/开始（user/add)   表示一个相对地址　
	 -->
	<form class="layui-form" action="/user?action=add" method="post"
		enctype="multipart/form-data">
		<div class="layui-form-item">
			<label class="layui-form-label">用户名</label>
			<div class="layui-input-inline">
				<input type="hidden" name="id" value="${user.id }"> <input
					type="text" name="username" lay-verify="required"
					value="${user.username }" autocomplete="off" placeholder="请输入用户名"
					class="layui-input">
			</div>
		</div>
		<div class="layui-form-item">
			<label class="layui-form-label">密码</label>
			<div class="layui-input-inline">
				<input type="password" name="password" lay-verify="required"
					value="${user.password }" placeholder="请输入" autocomplete="off"
					class="layui-input">
			</div>
		</div>

		<div class="layui-form-item">
			<div class="layui-inline">
				<label class="layui-form-label">确认密码</label>
				<div class="layui-input-inline">
					<input type="password" name="password2" autocomplete="off"
						placeholder="请再次输入密码" class="layui-input">
				</div>
			</div>
		</div>
		<div class="layui-inline">
			<label class="layui-form-label">生日</label>
			<div class="layui-input-inline">
				<input type="date" name="birthday" id="date"
					value="${user.birthday }" placeholder="yyyy-MM-dd"
					autocomplete="off" class="layui-input">
			</div>
		</div>
		<div class="layui-form-item">
			<div class="layui-inline">
				<label class="layui-form-label">年龄</label>
				<div class="layui-input-inline">
					<input type="text" name="age" autocomplete="off"
						value="${user.age }" placeholder="请输入年龄" class="layui-input">
				</div>
			</div>
		</div>
		<div class="layui-form-item">
			<label class="layui-form-label">昵称</label>
			<div class="layui-input-inline">
				<input type="text" name="name" value="${user.name }"
					lay-verify="title" autocomplete="off" placeholder="请输入昵称"
					class="layui-input">
			</div>
		</div>
		<div class="layui-inline">
			<label class="layui-form-label">邮箱</label>
			<div class="layui-input-inline">
				<input type="text" name="email" value="${user.email }"
					lay-verify="email" autocomplete="off" class="layui-input">
			</div>
		</div>
		<br /> <br />

		<div class="layui-form-item">
			<label class="layui-form-label">性别</label>
			<div class="layui-input-block">

				<input type="radio" name="gender" value="男" title="男"
					<c:if test="${user.gender=='男'}">checked="checked"</c:if>>
				<input type="radio" name="gender" value="女" title="女"
					<c:if test="${user.gender=='女'}">checked="checked"</c:if>>
			</div>
		</div>
		

		<div class="layui-form-item">
			<label class="layui-form-label">角色</label>
			<div class="layui-input-inline">
			
				<select name="role">
					 
					<option value="0">请选择角色</option>

					<c:forEach items="${roles }" var="r" varStatus="s">


						<option value="${r.id }" <c:if test="${user.roleId==r.id}">selected="selected"</c:if>>${r.name }</option>


					</c:forEach>
				</select>
			</div>
		</div>
		<div class="layui-form-item" style="padding-left: 100px">
			<label>头像</label> <img id="pic" src=""> <input id="upload"
				name="avatar" accept="image/*" type="file" style="display: none" />
		</div>
		<div class="layui-form-item">
			<div class="layui-input-block">
				<button type="submit" onclick="return f2()" class="layui-btn"
					lay-submit="" lay-filter="demo1">立即提交</button>
				<button type="reset" class="layui-btn layui-btn-primary">重置</button>
				<input type="button" onclick="history.back()" class="layui-btn"
					value="返回" />
			</div>
		</div>

	</form>
	<script src="/plugins/layui/layui.js"></script>

	<script>

	

	
	
			layui.use(['layer', 'form', 'element'], function(){
			  var layer = layui.layer,form = layui.form,element = layui.element;
			 });
			function f2(){
			
				console.log();
				if($("input[name='username']").val()==""&&$("input[name='username']").val()==null){
					layer.msg("用户名不对")
					return false;
				}
				
				var  password = $("input[name='password']").val();
				var  password2 = $("input[name='password2']").val();
				if(password==""&& password==null){
					layer.msg("密码不能为空")
					return false;
				}
				if($("input[name='password']").val().length<8||$("input[name='password']").val().length>16){
					layer.msg("密码长度请输入8到16位的")
					return false;
				}
				if(password2==""&& password2==null){
					layer.msg("确认密码不能为空")
					return false;
				}
				if(password!=password2){
					layer.msg("密码和确认密码不一致")
					return false;
				}
				return true;
				
			}
			 $(function() {
				 $("#pic").click(function () {
				 $("#upload").click(); //隐藏了input:file样式后，点击头像就可以本地上传
				 $("#upload").on("change",function(){
				 var objUrl = getObjectURL(this.files[0]) ; //获取图片的路径，该路径不是图片在本地的路径
				 if (objUrl) {
				 $("#pic").attr("src", objUrl) ; //将图片路径存入src中，显示出图片
				 }
				 });
				 });
				 });

				 //建立一個可存取到該file的url
				 function getObjectURL(file) {
				 var url = null ;
				 if (window.createObjectURL!=undefined) { // basic
				 url = window.createObjectURL(file) ;
				 } else if (window.URL!=undefined) { // mozilla(firefox)
				 url = window.URL.createObjectURL(file) ;
				 } else if (window.webkitURL!=undefined) { // webkit or chrome
				 url = window.webkitURL.createObjectURL(file) ;
				 }
				 return url ;
				 }
					
				 
		</script>
</body>
</html>