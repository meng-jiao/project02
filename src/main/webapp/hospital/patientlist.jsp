<%@page import="com.situ.entity.User"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	isELIgnored="false" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" href="/plugins/layui/css/layui.css" media="all" />
<style type="text/css">
.select{
  margin-top:8px;
  margin-left:8px;
  background: rgba(0,0,0,0,2);
  width: 100px;
  height:25px;
  birder-radius:10px;
}

</style>
</head>
<body>

	<form action="/patient?action=list" method="post" >
		<div class="layui-form-item">
			<div class="layui-inline">
				<label class="layui-form-label">姓名</label>
				<div class="layui-input-inline">
					<input type="text" name="name" autocomplete="off"
						class="layui-input">
				</div>
				<label class="layui-form-label">住院号</label>
				<div class="layui-input-inline">
					<input type="text" name="inpatientno" autocomplete="off" class="layui-input">
					<input id="pageSize" type="hidden"	name="pageSize" value="3" autocomplete="off" class="layui-input">
					<input id="pageNum" type="hidden" name="pageNum" value="1"	autocomplete="off" class="layui-input">	
				</div>

				<label class="layui-form-label">主管医生</label>
					<div class="layui-input-inline" >

						<select class="select" name="doctorId" id="doctorId" lay-verify="">
						
						</select>
					</div>
					
					<label class="layui-form-label">病房号</label>
					<div class="layui-input-inline" >

						<select class="select" name=wardId id="wordId" lay-verify="">
						
						</select>
					</div>
				
					

					
				<input id="searchBtn" type="submit" class="layui-btn" value="查询">

				<a href="/patient?action=addskip" class="layui-btn">新增</a>

				
			
					
			</div>
		</div>
	</form>
	<table id="userTable" class="layui-table">

		<thead>
			<tr>
				<th>序号</th>
				<th>姓名</th>
				<th>性别</th>
				<th>年龄</th>
				<th>住院号</th>
				<th>主管医生</th>
				<th>病房号</th>
				<th>操作</th>
			</tr>
		</thead>
		<tbody>

		
			<c:forEach items="${patients }" var="p" varStatus="s">
				<tr>
					<td>${s.count }</td>
					<td>${p.name}</td>
					<td>${p.gender}</td>
					<td>${p.age}</td>
					<td>${p.inpatientno}</td>
					<td> ${p.doctorName} </td>					
					<td>${p.wardName}</td>
					<td><a href="/patient?action=update&id=${p.id }"
						class="layui-btn layui-btn-xs delBtn">编辑</a>
					 <a	href = "javascript:;" id="adminDelete" onclick="return f1(${p.id})"
						class="layui-btn layui-btn-xs delBtn">删除</a></td>
				</tr>

			</c:forEach>




		</tbody>
	</table>
	<dir id="page">
	</dir>
	<script type="text/javascript"
		src="<%=request.getContextPath()%>/plugins/layui/layui.js"></script>



	<script>
	
	
	
	var $ = null;
		layui.use([ 'table', 'form', 'laypage' ], function() {
			var table = layui.table;
			var form = layui.form;
			 $ = layui.jquery;

			var laypage = layui.laypage;
			var abc = "${abc}";
			var sum =  "${sum}";
			var pageNum = "${pageNum}";
			var pageSize = "${pageSize}";
			if (abc == null || abc == "") {
				//
				$("#searchBtn").click();//searchBtn的ID
			}
			laypage.render({
				elem : 'page',  //elem是表示要渲染那个
				
				count : sum ,//表示一共有多少数据
			
				layout : [ 'limit', 'prev', 'page', 'next', 'count' ],
				limit :pageSize ,
				limits : [ 3, 10, 20, 30, 40 ],
				curr: pageNum
				,
				jump : function(obj, first) {//first表示第一次过来
					if (!first) {//首次不执行
						//obj的curr参数是第几页，obj其中的limit是表示几条一页
						$("#pageNum").val(obj.curr);
						$("#pageSize").val(obj.limit);
						$("#searchBtn").click();
					}

				}
			});
			
			
			$.get("/doctor?action=selectDoctorAll",function(ret){//主管医生的的select
				var html=" <option value=''>请选择主管医生</option> ";
				
				$.each(ret,function(i,v){//i是index，v是表示里面的遍历的里面的对象
										//拼接
					html += " <option value='"+v.id+"'>"+v.name+"</option> ";
				});
				$("#doctorId").html(html);
			});
			$.get("/ward?action=selectWardAll",function(ret){//病房的select
				var html=" <option value=''>请选择病房</option> ";
				
				$.each(ret,function(i,v){//i是index，v是表示里面的遍历的里面的对象
										//拼接
					html += " <option value='"+v.id+"'>"+v.wardno+"</option> ";
				});
				$("#wordId").html(html);
			});
			

		});
		function f1(patient) {
			layer.confirm("你确定要删除该用户？",
			function(){
				//ajax
				$.get("/patient?action=del&id="+patient,function(){
					$("#searchBtn").click();
					layer.closeAll();//取消所有的框
				})
				
			},
			function(){
				console.log("点击了取消");
			}
			);
		}
	</script>

</body>
</html>