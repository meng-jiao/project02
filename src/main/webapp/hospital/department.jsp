<%@page import="com.situ.entity.User"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	isELIgnored="false" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" href="/plugins/layui/css/layui.css" media="all" />

</head>
<body>

	<form action="/dept?action=list" method="post">
		<div class="layui-form-item">
			<div class="layui-inline">

				<label class="layui-form-label">名称</label>
				<div class="layui-input-inline">
					<input type="text" name="name" autocomplete="off"
						class="layui-input">
						 <input id="pageSize" type="hidden"
						name="pageSize" value="3" autocomplete="off" class="layui-input">
					<input id="pageNum" type="hidden" name="pageNum" value="1"
						         autocomplete="off" class="layui-input">
				</div>





				<input id="searchBtn" type="submit" class="layui-btn" value="查询">

				<a href="/hospital/deptadd.jsp" class="layui-btn">新增</a>

			</div>
		</div>
	</form>
	<table id="userTable" class="layui-table">

		<thead>
			<tr>
				<th>序号</th>
				<th>名称</th>
				<th>电话</th>
				<th>地址</th>
				<th>操作</th>
			</tr>
		</thead>
		<tbody>

			<!-- JSP标准标签库（java Server  Pages Standard Tag Library） 
		简称JSTL
		功能：同样是为了简化我们的jsp代码，一般与EL表达式结合使用
		EL表达式多用于取值，		JSTL则方便我们对集合进行遍历，对数据进行判读等操作
		
		JSTL引入：导包，用 taglib引入

			taglib:	作用：	在页面中，引入自定义标签

					属性：

						prefix:	自定义标签的前缀；

						uri：	引入的是哪个自定义标签
			
				c:set 将值保存在指定作用域中
				c:out 将结果输出	
		<c:set var="aaa" value="a1" scope="request" ></c:set>
	${aaa }
	<c:out value="${aaa }"></c:out>
		
	-->
			<!-- 起了个叫aaa的变量   值为a1   score是设置其作用域 -->


			<!-- 注意：items表示要遍历的那个集合，那么我们就用EL找到那个集合 -->
			<c:forEach items="${depts }" var="v" varStatus="s">
				<tr>
					<td>${s.count }</td>

					<td>${v.name}</td>
					<td>${v.tel}</td>
					<td>${v.address}</td>
					<td><a href="/dept?action=update&id=${v.id }"
						class="layui-btn layui-btn-xs delBtn">编辑</a> <a
						href="javascript:;" id="adminDelete" onclick="return f1(${v.id})"
						class="layui-btn layui-btn-xs delBtn">删除</a></td>
				</tr>

			</c:forEach>




		</tbody>
	</table>
	<dir id="page">
	</dir>
	<script type="text/javascript"
		src="<%=request.getContextPath()%>/plugins/layui/layui.js"></script>



	<script>
	
	
	
	var $ = null;
		layui.use([ 'table', 'form', 'laypage' ], function() {
			var table = layui.table;
			var form = layui.form;
			 $ = layui.jquery;

			var laypage = layui.laypage;
			var abc = "${abc}";
			var sum =  "${sum}";
			var pageNum = "${pageNum}";
			var pageSize = "${pageSize}";
			if (abc == null || abc == "") {
				//
				$("#searchBtn").click();//searchBtn的ID
			}
			laypage.render({
				elem : 'page',  //elem是表示要渲染那个
				
				count : sum ,//表示一共有多少数据
			
				layout : [ 'limit', 'prev', 'page', 'next', 'count' ],
				limit :pageSize ,
				limits : [ 3, 10, 20, 30, 40 ],
				curr: pageNum
				,
				jump : function(obj, first) {//first表示第一次过来
					if (!first) {//首次不执行
						//obj的curr参数是第几页，obj其中的limit是表示几条一页
						$("#pageNum").val(obj.curr);
						$("#pageSize").val(obj.limit);
						$("#searchBtn").click();
					}

				}
			});
			

		});
		function f1(deatId) {
			layer.confirm("你确定要删除该用户？",
			function(){
				//ajax
				$.get("/dept?action=del&id="+deatId,function(){
					$("#searchBtn").click();
					layer.closeAll();//取消所有的框
				})
				
			},
			function(){
				console.log("点击了取消");
			}
			);
		}
	</script>

</body>
</html>