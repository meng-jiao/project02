<%@page import="com.situ.entity.User"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	isELIgnored="false" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link href="/plugins/layui/css/layui.css" rel="stylesheet"
	type="text/css" />
<script type="text/javascript" src="/js/jquery-3.6.0.js"></script>
<style type="text/css">
#pic {
	width: 100px;
	height: 100px;
	border-radius: 50%;
	margin: 20px auto;
	cursor: pointer;
}
</style>
</head>
<body>
	<!-- 
		action:   /**(/user/add)   表示请求地址是　　http://ip:端口号/action (http://localhost:8080/user/add　少项目名)
		
		如果　不写/开始（user/add)   表示一个相对地址　
	 -->
	<form class="layui-form" action="/dept?action=add" method="post">
		
		<div class="layui-form-item">
			<label class="layui-form-label">名称</label>
			<div class="layui-input-inline">
			<input type="hidden" name="id" value="${dept.id }">
				<input type="text" name="name" value="${dept.name }"
					lay-verify="title" autocomplete="off" placeholder="请输入名称"
					class="layui-input">
			</div>
		</div>
		
	

		<div class="layui-form-item">
			<label class="layui-form-label">电话号码</label>
			<div class="layui-input-inline">
				<input type="text" name="tel" value="${dept.tel }"
					lay-verify="title" autocomplete="off" placeholder="请输入电话号码"
					class="layui-input">
			</div>
		</div>
		
		<div class="layui-form-item">
			<label class="layui-form-label">地址</label>
			<div class="layui-input-inline">
				<input type="text" name="address" value="${dept.address }"
					lay-verify="title" autocomplete="off" placeholder="请输入地址"
					class="layui-input">
			</div>
		</div>

		
		<div class="layui-form-item">
			<div class="layui-input-block">
				<button type="submit" onclick="return f2()" class="layui-btn"
					lay-submit="" lay-filter="demo1">立即提交</button>
				<button type="reset" class="layui-btn layui-btn-primary">重置</button>
				<input type="button" onclick="history.back()" class="layui-btn"
					value="返回" />
			</div>
		</div>

	</form>
	<script src="/plugins/layui/layui.js"></script>

	<script>

	

	
	
			layui.use(['layer', 'form', 'element'], function(){
			  var layer = layui.layer,form = layui.form,element = layui.element;
			 });
			function f2(){
			
				console.log();
				if($("input[name='name']").val()==""&&$("input[name='name']").val()==null){
					layer.msg("名称不能为空")
					return false;
				}
				
				var  tel = $("input[name='tel']").val();
				
				
				if(tel.length!=11){
					layer.msg("电话号码错误！");
					return false;
				}
			}
				 
		</script>
</body>
</html>