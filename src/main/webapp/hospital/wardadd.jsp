<%@page import="com.situ.entity.User"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	isELIgnored="false" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link href="/plugins/layui/css/layui.css" rel="stylesheet"
	type="text/css" />
<script type="text/javascript" src="/js/jquery-3.6.0.js"></script>
<style type="text/css">
#pic {
	width: 100px;
	height: 100px;
	border-radius: 50%;
	margin: 20px auto;
	cursor: pointer;
}
</style>
</head>
<body>
	<!-- 
		action:   /**(/user/add)   表示请求地址是　　http://ip:端口号/action (http://localhost:8080/user/add　少项目名)
		
		如果　不写/开始（user/add)   表示一个相对地址　
	 -->
	<form class="layui-form" action="/ward?action=add" method="post">
		
		<div class="layui-form-item">
			<label class="layui-form-label">病房号</label>
			<div class="layui-input-inline">
			<input type="hidden" name="id" value="${ward.id }">
				<input type="text" name="wardno" value="${ward.wardno }"
					lay-verify="title" autocomplete="off" placeholder="请输入病房号"
					class="layui-input">
			</div>
		</div>
		
	

		<div class="layui-form-item">
			<label class="layui-form-label">地址</label>
			<div class="layui-input-inline">
				<input type="text" name="address" value="${ward.address }"
					lay-verify="title" autocomplete="off" placeholder="请输入地址"
					class="layui-input">
			</div>
		</div>
		
		
		
		<div class="layui-form-item">
			<label class="layui-form-label">科室</label>
			<div class="layui-input-inline">
			
				<select name="deptId">
					 
					<option value="0">请选择科室</option>

					<c:forEach items="${depts }" var="d" varStatus="s">


						<option value="${d.id }" <c:if test="${ward.deptId==d.id}">selected="selected"</c:if>>${d.name }</option>


					</c:forEach>
				</select>
			</div>
		</div>
		
		
		
		<div class="layui-form-item">
			<div class="layui-input-block">
				<button type="submit" onclick="return f2()" class="layui-btn"
					lay-submit="" lay-filter="demo1">立即提交</button>
				<button type="reset" class="layui-btn layui-btn-primary">重置</button>
				<input type="button" onclick="history.back()" class="layui-btn"
					value="返回" />
			</div>
		</div>

	</form>
	<script src="/plugins/layui/layui.js"></script>

	<script>

	

	
	
			layui.use(['layer', 'form', 'element'], function(){
			  var layer = layui.layer,form = layui.form,element = layui.element;
			 });
			function f2(){
			
				console.log();
				if($("input[name='wardno']").val().length==0){
					layer.msg("病房号不能为空")
					return false;
				}
				
				if($("input[name='address']").val().length==0){
					layer.msg("地址不能为空")
					return false;
				}
				
				
				
			}
				 
		</script>
</body>
</html>