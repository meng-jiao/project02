<%@ page language="java" contentType="text/html; charset=UTF-8"
  isELIgnored="false"   pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link href="./plugins/layui/css/layui.css" rel="stylesheet" type="text/css"/>
</head>
<body>

		<form class="layui-form" action="menu/add">
		  <div class="layui-form-item">
		    <label class="layui-form-label">菜单名</label>
		    <div class="layui-input-inline">
		      <input type="text" name="name" lay-verify="title" autocomplete="off" placeholder="请输入菜单名" class="layui-input">
		    </div>
		  </div>
		  <div class="layui-form-item">
		    <label class="layui-form-label">菜单地址</label>
		    <div class="layui-input-inline">
		      <input type="text" name="url" lay-verify="required"  placeholder="请输入菜单连接地址" autocomplete="off" class="layui-input">
		    </div>
		  </div>
		
		<div class="layui-form-item">
		    <div class="layui-inline">
		      <label class="layui-form-label">上级菜单ID</label>
		      <div class="layui-input-inline">
		        <input type="text" name="parentId" autocomplete="off"  placeholder="请确认上级菜单" class="layui-input">
		      </div>
		    </div>
			</div>
		
		<div class="layui-form-item">
		    <div class="layui-inline">
		      <label class="layui-form-label">菜单顺序</label>
		      <div class="layui-input-inline">
		        <input type="text" name="sequence" autocomplete="off"  placeholder="请填写菜单顺序" class="layui-input">
		      </div>
		    </div>
			</div>
		
		<div class="layui-form-item">
		    <div class="layui-input-block">
		      <button type="submit" class="layui-btn" lay-submit="" lay-filter="demo1">立即提交</button>
		      <button type="reset" class="layui-btn layui-btn-primary">重置</button>
		    </div>
		  </div>
		
		</form>
		<script src="./plugins/layui/layui.js"></script>
		<script>
			layui.use(['layer', 'form', 'element'], function(){
			  var layer = layui.layer,form = layui.form,element = layui.element;
			 });
		</script>

</body>
</html>