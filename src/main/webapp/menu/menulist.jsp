<%@page import="com.situ.entity.User"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
   isELIgnored="false"  pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" href="<%=request.getContextPath() %>/plugins/layui/css/layui.css" media="all" />

</head>
<body>

 <form action="<%=request.getContextPath() %>/user/list" method="post">
 <div class="layui-form-item">
 	<div class="layui-inline">
 	<label class="layui-form-label">菜单名</label>
 		<div class="layui-input-inline">
 		<input type="text" name="username" autocomplete="off" class="layui-input">
 		 </div>
 		 <input id="searchBtn" type="submit" class="layui-btn" value="查询">
 		 
 		<a  href="/user.jsp"   class="layui-btn">新增</a>
 		 
 		</div> 
 	</div> 
 	</form>
	<table id="userTable" class="layui-table"  >
	
	<thead >
		<tr>
		
		<th >菜单名</th>
		<th >菜单路径</th>
		<th >上级菜单</th>
		<th >序列</th>
		<th >操作</th>
		</tr>
	</thead>
	<tbody>
	
	<!-- JSP标准标签库（java Server  Pages Standard Tag Library） 
		简称JSTL
		功能：同样是为了简化我们的jsp代码，一般与EL表达式结合使用
		EL表达式多用于取值，		JSTL则方便我们对集合进行遍历，对数据进行判读等操作
		
		JSTL引入：导包，用 taglib引入

			taglib:	作用：	在页面中，引入自定义标签

					属性：

						prefix:	自定义标签的前缀；

						uri：	引入的是哪个自定义标签
			
				c:set 将值保存在指定作用域中
				c:out 将结果输出	
		<c:set var="aaa" value="a1" scope="request" ></c:set>
	${aaa }
	<c:out value="${aaa }"></c:out>
		
	-->
	<!-- 起了个叫aaa的变量   值为a1   score是设置其作用域 -->
	

	<!-- 注意：items表示要遍历的那个集合，那么我们就用EL找到那个集合 -->
		<c:forEach items="${menus }" var="u">
		<tr >
			<td> ${u.name}    </td>
			<td> ${u.url}  </td>
			<td> ${u.parentId}  </td>
			<td> ${u.sequence}  </td>
			<td>
			<a href="/menu/update?id=${u.id }" onclick="return confirm('确定要修改用户？')"  class="layui-btn layui-btn-xs delBtn">编辑</a>
			<a href="/menu/del?id=${u.id }" id="adminDelete" onclick="return confirm('确定将这个记录删除？')"  class="layui-btn layui-btn-xs delBtn">删除</a>
			</td>
		 </tr>
		
		</c:forEach>


		
	
	</tbody>
	</table>
	
		<script type="text/javascript" src="<%=request.getContextPath() %>/plugins/layui/layui.js"></script>
		
		<script >
	
		layui.use(['table','form'],function(){
			var table = layui.table;
			var form = layui.form;
			var $ = layui.jquery;
			var abc = "${abc}";
		
			
		});
		
		</script>
		
</body>
</html>