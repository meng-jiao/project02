<%@ page language="java" contentType="text/html; charset=UTF-8"
	isELIgnored="false" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<title>后台管理模板</title>
<meta name="renderer" content="webkit">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1">
<meta name="apple-mobile-web-app-status-bar-style" content="black">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="format-detection" content="telephone=no">

<link rel="stylesheet"
	href="<%=request.getContextPath()%>/plugins/layui/css/layui.css"
	media="all" />
<link rel="stylesheet"
	href="<%=request.getContextPath()%>/css/global.css" media="all">
<link rel="stylesheet" type="text/css"
	href="http://www.jq22.com/jquery/font-awesome.4.6.0.css">

</head>
<body>
<body>
	<div class="layui-layout layui-layout-admin">
		<div class="layui-header header header-demo">

			<div class="layui-main">
				<div class="admin-login-box">
					<a class="logo" style="left: 0;" href="#"> <span
						style="font-size: 22px;">系统管理</span>
					</a>
					<div class="admin-side-toggle">
						<i class="fa fa-bars" aria-hidden="true"></i>
					</div>
				</div>
				<ul class="layui-nav admin-header-item">
					<li class="layui-nav-item"><a href="javascript:;"
						class="admin-header-user"> <img
							src="<%=request.getContextPath()%>/img/0.jpg" /> <span>
								${curr.name }  </span>
					</a> <!-- 从jsp2.0版本开始引入了Expression  Language (表达式语言)简称EL表达式 
							
							EL表达式： 是一种在jsp页面获取数据的简单方式，（只能获取数据，不能设置数据）
							语法：${表达式}
							EL表达式访问javaBean
							javaBean是公共的类并且满足条件：
							1，有一个public的默认的构造方法
							2，属性要有public的get  set方法并且命名要规范
							3，类要实现序列化
							访问javaBean 实际上底层就是调用了对象的getXXX方法
							
							EL表达式访问数组或者集合（list）时
							访问方式：名[下标]
							同时，要注意：el不会出异常，获取不到就什么也不显示，比如获取3个元素的数组中的第4个数
										并且是null，也是不显示，封装的特别死
							
							EL表达式访问map:
							${map的名字.key}
							
							EL里的运算符：	
							${user.age+10}
							算数运算符：有+ - * / %  没有++和--
							关系运算符： ==  !=   > <  >=  <=
							逻辑运算符：&&  ||  ！  and   or   not
							条件(三目):  ?:
							
							empty 运算符 ${empty user.name} 返回一个布尔值
							在EL中empty对""和null的处理都返回true，而==null对""返回false，对null返回true。
							
							
							--> <!-- EL表达式是从哪里获取出来的？
									是从request里面拿到的
									不过他不只能在request里面拿值
									还有
									
									page 	 页面：		作用域： 只在这一个页面上有效
									
									request  请求  		作用域： 只在一次请求中有效
									
									session  会话		作用域：客户端与服务端建立的通信会话
									
									application  程序   	作用域： 这一个服务程序
									
									四大作用域：
									${a}  查找顺序： page-> request->session->applocation
							 -->



						<dl class="layui-nav-child">
							<dd>
								<a href="/user/login.jsp"><i
									class="fa fa-sign-out" aria-hidden="true"></i> 注销</a>
							</dd>
						</dl></li>
				</ul>
			</div>
		</div>

		<!-- sider -->
		<div class="layui-side layui-bg-black" id="admin-side">
			<div class="layui-side-scroll" id="admin-navbar-side"
				lay-filter="side"></div>
		</div>


		<!-- center -->
		<div class="layui-body"
			style="bottom: 0; border-left: solid 2px #1AA094;" id="admin-body">
			<div class="layui-tab admin-nav-card layui-tab-brief"
				lay-filter="admin-tab">
				<ul class="layui-tab-title">
					<li class="layui-this"><i class="fa fa-dashboard"
						aria-hidden="true"></i> <cite>主页</cite></li>
				</ul>
				<div class="layui-tab-content"
					style="min-height: 150px; padding: 5px 0 0 0;">
					<div class="layui-tab-item layui-show">
						<iframe src=""></iframe>
					</div>
				</div>
			</div>
		</div>


		<!-- footer -->
		<div class="layui-footer footer footer-demo" id="admin-footer">
			<div class="layui-main">
				<p>
					2016 &copy; <a href="#">situedu</a> LGPL license
				</p>
			</div>
		</div>
		<div class="site-tree-mobile layui-hide">
			<i class="layui-icon">&#xe602;</i>
		</div>
		<div class="site-mobile-shade"></div>

		<script type="text/javascript"
			src="<%=request.getContextPath()%>/plugins/layui/layui.js"></script>

		<script src="<%=request.getContextPath()%>/js/index.js"></script>
	</div>
</body>
</html>