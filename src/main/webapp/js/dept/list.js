layui.use([ 'table', 'layer' ], function() {
	var table = layui.table;
	var $ = layui.jquery;
	var layer = layui.layer;
	// 表格中的toolbar被点击时的事件
	table.on('tool(deptTable)', function(obj) {
		if (obj.event == "edit") {
			window.location.href = "/dept?action=edit&id=" + obj.data.id;
		}
		if (obj.event == "del") {
			layer.confirm("你确定要删除该科室吗？", function() {
				$.get("/dept?action=del&id=" + obj.data.id, function() {
					$("#searchBtn").click();
					layer.closeAll();
				});
			}, function() {});
			
		}
	});

	// 查询按钮绑定事件
	$("#searchBtn").on("click", function() {
		table.render({
			elem : "#deptTable",
			cols : [[{
				type : 'numbers',
				title : '序号',
				align : 'center',
				width : '10%'
			}, {
				field : 'name',
				title : '名称',
				width : '20%',
				align : 'center'
			}, {
				field : 'tel',
				title : '电话',
				width : '20%',
				align : 'center'
			}, {
				field : 'address',
				title : '地址',
				width : '30%',
				align : 'center'
			}, {
				title : '操作',
				toolbar :'#barDemo',
				width : '20%',
				align : 'center'
			}, {
				field : 'id',
				title : 'ID',
				hide : true
			}]],
			limit : 5,
			limits : [ 5, 10, 15, 20, 25, 30 ],
			layout : [ 'limit', 'prev','page', 'next', 'count' ],
			url : "/dept",
			where : {action : "asyncList",
			name : $("#name").val()},
			page : true
		});
	}).click();
	$("#addBtn").on("click",function() {
		window.location.href = "/hospital/deptadd.jsp";
	});
});


	
	
	
	
	
	
