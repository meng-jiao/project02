package com.situ.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.situ.entity.User;

//登录相关的filter
//@WebFilter  可以在里面配置映射地址
@WebFilter(urlPatterns = "/*")
public class LoginFilter implements Filter {

	@Override
	public void destroy() {// 结束方法
		// TODO Auto-generated method stub

	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {// 执行过滤
		// TODO Auto-generated method stub
		HttpServletRequest req = (HttpServletRequest) request;// 强制转为HttpServletRequest类型
		HttpServletResponse resp = (HttpServletResponse) response;
		String uri = req.getRequestURI();// 查看当前请求的地址或者资源

		System.out.println(uri);
		if (uri.startsWith("/css/") || uri.startsWith("/js/") || uri.startsWith("/plugins/") || uri.contains("login")) {
			// 如果是以0这个几个开头的，直接放行 startsWith用于检测字符串是否以指定的前缀开始。
			chain.doFilter(request, response);

		} else {

			User user = (User) req.getSession().getAttribute("curr");// 拿到当前请求的session对话

			boolean isLongin = (user != null);// 是否登录
			if (isLongin) {
				chain.doFilter(request, response);
			} else {
				
				resp.sendRedirect("/user/login.jsp");
			}

		}

	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {// 初始化
		// TODO Auto-generated method stub
		System.out.println("long filter init...");

	}

}
