package com.situ.service;

import java.util.List;

import com.situ.entity.Department;
import com.situ.entity.Doctor;
import com.situ.entity.PageEntity;
import com.situ.entity.Role;


public interface IDepartmentService {

	
	List<Department> selectlist(String name, PageEntity page);

	Integer countDept(String name);

	void update(Department dept);

	void add(Department dept);

	Department seleDept(Integer id);

	void deletDepat(Integer id);

	List<Department> selectdeptAll();

}
