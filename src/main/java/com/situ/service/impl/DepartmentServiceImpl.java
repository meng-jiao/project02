package com.situ.service.impl;

import java.util.List;

import com.situ.dao.DepartmentDao;
import com.situ.entity.Department;
import com.situ.entity.PageEntity;
import com.situ.service.IDepartmentService;


public class DepartmentServiceImpl implements IDepartmentService {



	@Override
	public List<Department> selectlist(String name, PageEntity page) {
		// TODO Auto-generated method stub
		 return DepartmentDao.selectUser(name,(page.getPageNum()-1)*page.getPageSize(),page.getPageSize());
	}

	@Override
	public Integer countDept(String name) {
		// TODO Auto-generated method stub
		return  DepartmentDao.countDept(name);
	}

	@Override
	public void update(Department dept) {
		// TODO Auto-generated method stub
		DepartmentDao.update(dept);
	}

	@Override
	public void add(Department dept) {
		// TODO Auto-generated method stub
		DepartmentDao.add(dept);
	}

	@Override
	public Department seleDept(Integer id) {
		// TODO Auto-generated method stub
		
		List<Department> list =  DepartmentDao.seleDept(id);
		return list.get(0);
	}

	@Override
	public void deletDepat(Integer id) {
		// TODO Auto-generated method stub
		DepartmentDao.deletDepat(id);
	}

	@Override
	public List<Department> selectdeptAll() {
		// TODO Auto-generated method stub
		return DepartmentDao.selectdeptAll();
	}

}
