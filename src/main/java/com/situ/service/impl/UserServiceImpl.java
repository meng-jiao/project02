package com.situ.service.impl;

import java.util.List;

import com.situ.dao.UserDao;
import com.situ.entity.User;
import com.situ.service.IUserService;
import com.situ.util.StringUtil;

public class UserServiceImpl implements IUserService {

	UserDao userDao = new UserDao();
	
	@Override
	public void addUser(User user) {
		userDao.addUser(user);
	}

	@Override
	public User checkUser(String username, String password) {
		// TODO Auto-generated method stub
		List<User> list =	userDao.check(username,password);
		if (list == null || list.size() == 0) {
			return null;
		}
		return list.get(0);
	}

	@Override
	public Integer countUsers(String username, String name) {
		// TODO Auto-generated method stub
		try {
			return userDao.countUsers(username, name);
		} catch (InstantiationException | NoSuchFieldException | IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return 0;
	}
	
	
	@Override
	public List<User> selectUsers(String username,String name,Integer pageSize, Integer pageNum) {
		// TODO Auto-generated method stub
		//在userDao.selectUser方法中对有没有用户名和姓名进行判空
			return userDao.selectUser(username,name,(pageNum-1)*pageSize,pageSize);
	
		
	}

	@Override
	public void userIdDele(Integer id) {
		// TODO Auto-generated method stub
		userDao.userIdDele(id);
		
	}

	@Override
	public User selectByid(String id) {
		// TODO Auto-generated method stub
	 List<User> list =	userDao.selectByid(id);
		if (list!=null ) {
			return list.get(0);
		}
	 return null;
	}

	@Override
	public void updateUser(User user) {
		// TODO Auto-generated method stub
		userDao.updateUser(user);
	}

	
	
	
	
	
}
