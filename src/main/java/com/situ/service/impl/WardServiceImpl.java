package com.situ.service.impl;

import java.util.List;

import com.situ.dao.WardDao;
import com.situ.entity.PageEntity;
import com.situ.entity.Ward;
import com.situ.service.IWardService;

public class WardServiceImpl implements IWardService {

	@Override
	public List<Ward> selectlist(String wardno, Integer deptId, PageEntity page) {
		// TODO Auto-generated method stub
		return WardDao.selectlist(wardno,deptId,(page.getPageNum()-1)*page.getPageSize(),page.getPageSize());
	}

	@Override
	public Integer countWard(String wardno, Integer deptId) {
		// TODO Auto-generated method stub
		return  WardDao.countWard(wardno,deptId);
	}

	@Override
	public void update(Ward ward) {
		// TODO Auto-generated method stub
		WardDao.update(ward);
	}

	@Override
	public void add(Ward ward) {
		// TODO Auto-generated method stub
		WardDao.add(ward);
	}

	@Override
	public Ward selectByid(Integer id) {
		// TODO Auto-generated method stub
		List<Ward> list = WardDao.selectByid(id);
		return list.get(0);
	}

	@Override
	public void deletWard(Integer id) {
		// TODO Auto-generated method stub
		WardDao.deletWard(id);
	}

	@Override
	public List<Ward> selectWardAll() {
		// TODO Auto-generated method stub
		return WardDao.selectWardAll() ;
	}

}
