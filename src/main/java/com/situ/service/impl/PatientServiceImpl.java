package com.situ.service.impl;

import java.util.List;

import com.situ.dao.PatientDao;
import com.situ.entity.PageEntity;
import com.situ.entity.Patient;
import com.situ.service.IPatientService;

public class PatientServiceImpl implements IPatientService {

	@Override
	public List<Patient> selectlist(String name, String inpatientno, Integer doctorId, Integer wardId,
			PageEntity page) {
		// TODO Auto-generated method stub
		return PatientDao.selectlist(name,inpatientno,doctorId,wardId,(page.getPageNum()-1)*page.getPageSize(),page.getPageSize());
	}

	@Override
	public Integer countPatient(String name, String inpatientno, Integer doctorId, Integer wardId) {
		// TODO Auto-generated method stub
		return PatientDao.countPatient(name,inpatientno,doctorId,wardId);
	}

	@Override
	public Patient selectById(Integer id) {
		// TODO Auto-generated method stub
	   List<Patient> list= PatientDao.selectById(id);
		return list.get(0);
	}

	@Override
	public void update(Patient patient) {
		// TODO Auto-generated method stub
		PatientDao.update(patient);
	}

	@Override
	public void add(Patient patient) {
		// TODO Auto-generated method stub
		PatientDao.add(patient);
	}

	@Override
	public void deletPatient(Integer id) {
		// TODO Auto-generated method stub
		PatientDao.deletPatient(id);
	}

}
