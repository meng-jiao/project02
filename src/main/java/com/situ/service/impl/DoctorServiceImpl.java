package com.situ.service.impl;

import java.util.List;

import com.situ.dao.DoctorDao;
import com.situ.entity.Doctor;
import com.situ.entity.PageEntity;
import com.situ.service.IDoctorService;

public class DoctorServiceImpl implements IDoctorService {

	@Override
	public List<Doctor> selectlist(String name, String cardno, Integer deptId,PageEntity pag) {
		// TODO Auto-generated method stub
		return DoctorDao.selectlist(name,cardno,deptId,(pag.getPageNum()-1)*pag.getPageSize(),pag.getPageSize());
	}

	@Override
	public Integer countDept(String name, String cardno, Integer deptId) {
		// TODO Auto-generated method stub
		return DoctorDao.countDept(name,cardno,deptId);
	}

	@Override
	public void update(Doctor doctor) {
		// TODO Auto-generated method stub
		DoctorDao.update(doctor);
	}

	@Override
	public void add(Doctor doctor) {
		// TODO Auto-generated method stub
		DoctorDao.add(doctor);
	}

	@Override
	public Doctor selectByid(Integer id) {
		// TODO Auto-generated method stub
		List<Doctor>  list = DoctorDao.selectByid(id);
		return list.get(0);
	}

	@Override
	public void deletDoctor(Integer id) {
		// TODO Auto-generated method stub
		DoctorDao.deletDoctor(id);
	}

	@Override
	public List<Doctor> selectDoctorAll() {
		// TODO Auto-generated method stub
		return DoctorDao.selectDoctorAll();
	}

}
