package com.situ.service;

import java.util.List;

import com.situ.entity.Doctor;
import com.situ.entity.PageEntity;
import com.situ.entity.Ward;

public interface IWardService {

	List<Ward> selectlist(String wardno, Integer deptId, PageEntity page);

	Integer countWard(String wardno, Integer deptId);

	void update(Ward ward);

	void add(Ward ward);

	Ward selectByid(Integer id);

	void deletWard(Integer id);

	List<Ward> selectWardAll();

}
