package com.situ.service;

import java.util.List;

import com.situ.entity.Menu;

public interface IMenuService {

	/**
	 * 添加菜单
	 * @param menu
	 * @return
	 */
	int reg(Menu menu);

	/**
	 * 查询菜单，查询你这个菜单下的菜单
	 * 
	 * @param menu
	 * @return
	 */
	List<Menu> selectMenu(Menu menu);

	/**
	 * 查询菜单 ，查询所有一级菜单
	 * 
	 * @return
	 */
	List<Menu> selectOne();

	/**
	 * 删除菜单
	 * 
	 * @param name
	 * @return
	 */
	int deletMenu(String name);
}
	

