package com.situ.service;

import java.util.List;

import com.situ.entity.PageEntity;
import com.situ.entity.Patient;

public interface IPatientService {

	List<Patient> selectlist(String name, String inpatientno, Integer doctorId, Integer wardId, PageEntity page);

	Integer countPatient(String name, String inpatientno, Integer doctorId, Integer wardId);

	Patient selectById(Integer id);

	void update(Patient patient);

	void add(Patient patient);

	void deletPatient(Integer id);

}
