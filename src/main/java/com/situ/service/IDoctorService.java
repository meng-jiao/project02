package com.situ.service;

import java.util.List;

import com.situ.entity.Doctor;
import com.situ.entity.PageEntity;

public interface IDoctorService {

	List<Doctor> selectlist(String name, String cardno, Integer deptId, PageEntity page);

	Integer countDept(String name, String cardno, Integer deptId);

	void update(Doctor doctor);

	void add(Doctor doctor);

	Doctor selectByid(Integer id);

	void deletDoctor(Integer id);

	List<Doctor> selectDoctorAll();

}
