package com.situ.service;

import java.util.List;

import com.situ.entity.User;

public interface IUserService {
	/**
	 * 添加用户
	 * @param user
	 */
	void addUser(User user);
	/**
	 * 根据username和password查询用户
	 * @param username
	 * @param password
	 * @return
	 */
	User checkUser(String username, String password);
	/**
	 * 更加username模糊查询
	 * @param username
	 * @param pageNum 
	 * @param pageSize 
	 * @return
	 */
	List<User> selectUsers(String username,String name, Integer pageSize, Integer pageNum);
	void userIdDele(Integer id);
	
	User selectByid(String id);
	
	void updateUser(User user);
	Integer countUsers(String username, String name);

}
