package com.situ.servlet;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Date;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import com.situ.entity.Role;
import com.situ.entity.User;
import com.situ.service.IRoleService;
import com.situ.service.IUserService;
import com.situ.service.impl.RoleServiceImpl;
import com.situ.service.impl.UserServiceImpl;

//用户相关的sevlet
@WebServlet("/user")
@MultipartConfig
public class UserServlet extends HttpServlet {

	private static final long serialVersionUID = 2223554844504036989L;

	IRoleService roleService = new RoleServiceImpl();

	IUserService userService = new UserServiceImpl();
	// 两个参数
	// HttpServletRequest request 请求

	// HttpServletResponse response 响应

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		String action = request.getParameter("action");
		// 区分用户的操作
		switch (action) {
		case "add":
			add(request, response);
			break;
		case "addskip":
			addSkip(request, response);
			break;
		
		case "del":
			del(request, response);
			break;
		case "update":
			update(request, response);
			break;
		case "list":
			list(request, response);
			break;

		}
	}

	

	private void list(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String username = request.getParameter("username");//
		String name = request.getParameter("name");//
		// 每页几条
		Integer pageSize = Integer.parseInt(request.getParameter("pageSize"));
		// 第几页
		Integer pageNum = Integer.parseInt(request.getParameter("pageNum"));

		Integer sum = userService.countUsers(username, name);

		System.out.println(sum);
		// 调用方法查询出结果
		List<User> list = userService.selectUsers(username, name, pageSize, pageNum);

		// 把结果放到request
		request.setAttribute("users", list);
		request.setAttribute("abc", "abc");// 这个用于自动查询一下，如果这是进行了查询跳转到userlist.jsp页面的话，abc是有值（abc），
											// 如果他不是通过查询操作进去的，abc为空,
											// abc在userlist.jsp页面,有个判断，判断为空，进行一次点击查询，查询后就有了abc的值
											// 这样的话，就会只进行了一次点击查询事件，这样的话只要是abc是空的就进行一次查询，查询一次后
		request.setAttribute("pageNum", pageNum);
		request.setAttribute("pageSize", pageSize);
		request.setAttribute("sum", sum);
		// 转发到用户列表，请求转发
		request.getRequestDispatcher("/user/userlist.jsp").forward(request, response);
	}

	private void update(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		String id = request.getParameter("id");
		User user = userService.selectByid(id);
		request.setAttribute("user", user);
		
		Role role = new Role();

		List<Role> roles = roleService.selectRoleAll();

		request.setAttribute("roles", roles);
		request.getRequestDispatcher("/user.jsp").forward(request, response);

	}

	public void del(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String id = request.getParameter("id");
		userService.userIdDele(Integer.parseInt(id));
		request.getRequestDispatcher("/user/userlist.jsp").forward(request, response);
	}

	public void addSkip(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		

		List<Role> roles = roleService.selectRoleAll();

		request.setAttribute("roles", roles);

		request.getRequestDispatcher("/user.jsp").forward(request, response);

	}

	public void add(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		String id = request.getParameter("id");
		String username = request.getParameter("username");
		String name = request.getParameter("name");
		String password = request.getParameter("password");
		String email = request.getParameter("email");
		String gender = request.getParameter("gender");
		String birthday = request.getParameter("birthday");
		String roleId = request.getParameter("role");
		FileOutputStream fos = null;
		InputStream fis = null;
		User newuser =  (User) request.getSession().getAttribute("curr");// 拿到当前请求的session对话
		Integer newUserId = newuser.getId();
		
		Timestamp time= new Timestamp(System.currentTimeMillis());//获取系统当前时间 
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String timeStr = df.format(time); 
		time = Timestamp.valueOf(timeStr); 
		
		
		try {//
			Part part = request.getPart("avatar");
			fos = new FileOutputStream("E:\\mengjiao/a.png");
			fis = part.getInputStream();// part.getInputStream()拿到文件上传的流
			byte[] bs = new byte[1024];
			int len = 0;
			while ((len = fis.read(bs)) != -1) {
				fos.write(bs, 0, len);

			}
			fos.flush();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (ServletException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} finally {
			if (fis != null) {
				fis.close();
			}
			if (fos != null) {
				fos.close();
			}
		}

		String age = request.getParameter("age");
		User user = new User();
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		java.util.Date date = null;
		try {
			date = format.parse(birthday);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Date birthday1 = new Date(date.getTime());
		user.setBirthday(birthday1);
		user.setAge(Integer.parseInt(age));
		user.setUsername(username);
		user.setPassword(password);
		user.setGender(gender);
		user.setEmail(email);
		user.setName(name);
		user.setCreateby(newUserId);//添加人
		user.setCreateTime(time);//添加时间
		user.setRoleId(Integer.parseInt(roleId));
		if (id == null || id.equals("")) {
			userService.addUser(user);
		} else {
			user.setId(Integer.parseInt(id));
			userService.updateUser(user);
		}

		// userService.addUser()

		// response.setContentType("text/html;charset=UTF-8");
		// response.getWriter().write("成功！");
		request.getRequestDispatcher("/user/userlist.jsp").forward(request, response);

	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		doGet(req, resp);
	}

}
