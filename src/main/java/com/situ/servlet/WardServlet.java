package com.situ.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSON;
import com.situ.annotation.Param;
import com.situ.entity.Department;
import com.situ.entity.Doctor;
import com.situ.entity.PageEntity;
import com.situ.entity.Ward;
import com.situ.service.IDepartmentService;

import com.situ.service.IWardService;
import com.situ.service.impl.DepartmentServiceImpl;

import com.situ.service.impl.WardServiceImpl;
import com.situ.util.BaseServlet;
@WebServlet("/ward")
public class WardServlet extends BaseServlet {

	private static final long serialVersionUID = 2128198831936717412L;



       IWardService iWardService = new WardServiceImpl();
       IDepartmentService departmentService = new DepartmentServiceImpl();
	public void list( Ward ward ,PageEntity page, HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {
			List<Ward> list = iWardService.selectlist(ward.getWardno(),ward.getDeptId(),page);
		    Integer sum =  iWardService.countWard(ward.getWardno(),ward.getDeptId());
		    request.setAttribute("pageNum", page.getPageNum());
			request.setAttribute("pageSize", page.getPageSize());
			request.setAttribute("wards", list);
			request.setAttribute("sum", sum);
			request.setAttribute("abc", "abc");
			request.getRequestDispatcher("/hospital/wardlist.jsp").forward(request, response);
	}
   
	public void  addskip(HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {//把科室的信息传过去
		
		List<Department> depts = departmentService.selectdeptAll();

		request.setAttribute("depts", depts);

		request.getRequestDispatcher("/hospital/wardadd.jsp").forward(request, response);
		
	}
	public void update(@Param("id") Integer id,HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {
		// TODO Auto-generated method stub

				
		    Ward ward = iWardService.selectByid(id);
			request.setAttribute("ward", ward);
				
			List<Department> depts = departmentService.selectdeptAll();

			request.setAttribute("depts", depts);

			request.getRequestDispatcher("/hospital/wardadd.jsp").forward(request, response);
	}
	public void add(Ward ward,HttpServletRequest request, HttpServletResponse response  ) throws ServletException, IOException {
		if (ward.getId()!= null) {
			iWardService.update(ward);
		}else {
			iWardService.add(ward);
		}
	request.getRequestDispatcher("/hospital/wardlist.jsp").forward(request, response);
	}
	public void del(@Param("id")Integer id,HttpServletRequest request, HttpServletResponse response) {
		iWardService.deletWard(id);
		
	}
	public void  selectWardAll(HttpServletRequest request, HttpServletResponse response) throws IOException {

		response.setCharacterEncoding("UTF-8");
		
		response.setContentType("application/json; charset=UTF-8");
		//返回json
		List<Ward> list = iWardService.selectWardAll();
		// 然后用工具类把它转为json类型
		String listStr = JSON.toJSONString(list);
		response.getWriter().write(listStr);
	}

	
}
