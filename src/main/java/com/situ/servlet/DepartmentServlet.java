package com.situ.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSON;
import com.situ.annotation.Param;
import com.situ.entity.Department;
import com.situ.entity.PageEntity;
import com.situ.entity.Role;
import com.situ.service.IDepartmentService;
import com.situ.service.impl.DepartmentServiceImpl;
import com.situ.util.BaseServlet;
import com.situ.vo.ResultDataVO;

/**
 * Servlet implementation class Department
 */
@WebServlet("/dept")
public class DepartmentServlet extends BaseServlet{//继承这个BaseServlet会自动调用doGet  doPost方法

	
	private static final long serialVersionUID = -975870111710821927L;
	IDepartmentService departmentService = new DepartmentServiceImpl();
	
	public void list(@Param("name") String name, PageEntity page, HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {
		List<Department> list = departmentService.selectlist(name,page);
		Integer sum  = departmentService.countDept(name);
		request.setAttribute("pageNum", page.getPageNum());
		request.setAttribute("pageSize", page.getPageSize());
		request.setAttribute("depts", list);
		request.setAttribute("sum", sum);
		request.setAttribute("abc", "abc");
		request.getRequestDispatcher("/hospital/department.jsp").forward(request, response);
	}
	
	
	
	public void asyncList(@Param("name") String name, PageEntity page, HttpServletRequest request, HttpServletResponse response ) throws IOException {
		List<Department> list = departmentService.selectlist(name,page);
		Integer sum  = departmentService.countDept(name);
		ResultDataVO vo = new ResultDataVO();//规范：：//这个是要求返回格式规范的类，返回的必须是这个种格式
		vo.setCount(sum);
		vo.setData(list);
		response.setContentType("application/json;charset=utf-8");
		response.getWriter().write(JSON.toJSONString(vo));
	}
	public void add(Department dept, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println(dept.toString());
		if (dept.getId()!= null) {
			departmentService.update(dept);
		}else {
			departmentService.add(dept);
		}
		request.getRequestDispatcher("/hospital/department.jsp").forward(request, response);
	}
	public void del(@Param("id")Integer id,HttpServletRequest request, HttpServletResponse response) {
		departmentService.deletDepat(id);
		
	}
	public void update(@Param("id")Integer id,HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Department depa= departmentService.seleDept(id);
		request.setAttribute("dept", depa);
		request.getRequestDispatcher("/hospital/deptadd.jsp").forward(request, response);
	}
	 public void  selectdeptAll(HttpServletRequest request, HttpServletResponse response) throws IOException {
			response.setCharacterEncoding("UTF-8");
			// response.setContentType()的作用是使客户端浏览器，区分不同种类的数据，并根据不同的MIME调用浏览器内不同的程序嵌入模块来处理相应的数据。
			//例如web浏览器就是通过MIME类型来判断文件是GIF图片。通过MIME类型来处理json字符串。
			//response.setContentType()里面的参数作用：
//			text/html的意思是将文件的content-type设置为text/html的形式，浏览器在获取到这种文件时会自动调用html的解析器对文件进行相应的处理。
//	        text/plain的意思是将文件设置为纯文本的形式，浏览器在获取到这种文件时并不会对其进行处理。
		//  application/json  告诉服务器请求内容为json格式的字符串，服务器会对json进行解析         json字符串
			response.setContentType("application/json; charset=UTF-8");
			//返回json
			List<Department> list = departmentService.selectdeptAll();
			// 然后用工具类把它转为json类型
			String listStr = JSON.toJSONString(list);
			response.getWriter().write(listStr);
			
	    }

}
