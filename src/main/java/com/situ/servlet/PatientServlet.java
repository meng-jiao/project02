package com.situ.servlet;


import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.situ.annotation.Param;
import com.situ.entity.Doctor;
import com.situ.entity.PageEntity;
import com.situ.entity.Patient;
import com.situ.entity.Ward;
import com.situ.service.IDoctorService;
import com.situ.service.IPatientService;
import com.situ.service.IWardService;
import com.situ.service.impl.DoctorServiceImpl;
import com.situ.service.impl.PatientServiceImpl;
import com.situ.service.impl.WardServiceImpl;
import com.situ.util.BaseServlet;
@WebServlet("/patient")
public class PatientServlet extends BaseServlet {

	
	private static final long serialVersionUID = -1386075408577814019L;


       IWardService iWardService = new WardServiceImpl();//
       IDoctorService iDoctorService = new DoctorServiceImpl();
       IPatientService iPatientService = new PatientServiceImpl();
	public void list( Patient patient ,PageEntity page, HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException  {
			List<Patient> list = iPatientService.selectlist(patient.getName(),patient.getInpatientno(),patient.getDoctorId(),patient.getWardId(),page);
		    Integer sum =  iPatientService.countPatient(patient.getName(),patient.getInpatientno(),patient.getDoctorId(),patient.getWardId());
		    request.setAttribute("pageNum", page.getPageNum());
			request.setAttribute("pageSize", page.getPageSize());
			request.setAttribute("patients", list);
			request.setAttribute("sum", sum);
			request.setAttribute("abc", "abc");
			request.getRequestDispatcher("/hospital/patientlist.jsp").forward(request, response);
	}
   
	public void  addskip(HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {//把科室的信息传过去
		
		List<Ward> wards = iWardService.selectWardAll();

		request.setAttribute("wards", wards);
		
		List<Doctor> doctors = iDoctorService.selectDoctorAll();
		request.setAttribute("doctors", doctors);
		
		request.getRequestDispatcher("/hospital/patientadd.jsp").forward(request, response);
		
	}
	public void update(@Param("id") Integer id,HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {
		// TODO Auto-generated method stub

				
		    Patient patient = iPatientService.selectById(id);
			request.setAttribute("patient", patient);
				
			List<Ward> wards = iWardService.selectWardAll();

			request.setAttribute("wards", wards);
			
			List<Doctor> doctors = iDoctorService.selectDoctorAll();
			request.setAttribute("doctors", doctors);

			request.getRequestDispatcher("/hospital/patientadd.jsp").forward(request, response);
	}
	public void add(Patient patient,HttpServletRequest request, HttpServletResponse response  ) throws ServletException, IOException {
		if (patient.getId()!= null) {
			iPatientService.update(patient);
		}else {
			iPatientService.add(patient);
		}
	request.getRequestDispatcher("/hospital/patientlist.jsp").forward(request, response);
	}
	public void del(@Param("id")Integer id,HttpServletRequest request, HttpServletResponse response) {
		iPatientService.deletPatient(id);
		
	}

	

	
	
	
}
