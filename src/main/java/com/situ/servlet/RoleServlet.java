package com.situ.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSON;
import com.situ.entity.Role;
import com.situ.service.IRoleService;
import com.situ.service.impl.RoleServiceImpl;

@WebServlet("/role")
public class RoleServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7352040655238757034L;
	IRoleService roleService = new RoleServiceImpl();

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		String action = request.getParameter("action");
		switch (action) {
		case "selectRoleAll":

			selectRoleAll(request, response);
			break;

		default:
			break;
		}

	}

	public void selectRoleAll(HttpServletRequest request, HttpServletResponse response) throws IOException {
		response.setCharacterEncoding("UTF-8");
		// response.setContentType()的作用是使客户端浏览器，区分不同种类的数据，并根据不同的MIME调用浏览器内不同的程序嵌入模块来处理相应的数据。
		//例如web浏览器就是通过MIME类型来判断文件是GIF图片。通过MIME类型来处理json字符串。
		//response.setContentType()里面的参数作用：
//		text/html的意思是将文件的content-type设置为text/html的形式，浏览器在获取到这种文件时会自动调用html的解析器对文件进行相应的处理。
//        text/plain的意思是将文件设置为纯文本的形式，浏览器在获取到这种文件时并不会对其进行处理。
	//  application/json  告诉服务器请求内容为json格式的字符串，服务器会对json进行解析         json字符串
		response.setContentType("application/json; charset=UTF-8");
		//返回json
		List<Role> list = roleService.selectRoleAll();
		// 然后用工具类把它转为json类型
		String listStr = JSON.toJSONString(list);
		response.getWriter().write(listStr);
		
		
//		response.getWriter()返回的是PrintWriter，这是一个打印输出流。
//		response.getWriter().print(),不仅可以打印输出文本格式的（包括html标签），还可以将一个对象以默认的编码方式转换为二进制字节输出
//		response.getWriter().writer（）,只能打印输出文本格式的（包括html标签），不可以打印对象。

		
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		doPost(request, response);
	}

}
