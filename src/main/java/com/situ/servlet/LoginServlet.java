package com.situ.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.situ.entity.User;
import com.situ.service.IUserService;
import com.situ.service.impl.UserServiceImpl;

@WebServlet("/login")
public class LoginServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7772673371688057819L;

	IUserService userService = new UserServiceImpl();
	
	
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("UTF-8");
		String username = request.getParameter("username");
		String password = request.getParameter("password");

		User user = userService.checkUser(username, password);

		if (user != null) {// 正确
			
			//记录用户登录成功
			request.getSession().setAttribute("curr",user);//拿到当前请求的session对话
		
			// :重定向 request.getContextPath()获取项目名 因为/这个号会使想项目名丢失，所以前面拼接上
			// response.sendRedirect(request.getContextPath()+"/index.jsp");
			
			request.setAttribute("currentUser", user);

			request.getRequestDispatcher("/index.jsp").forward(request, response);

		}

	}
		





	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}


	