package com.situ.servlet;


import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSON;
import com.situ.annotation.Param;
import com.situ.entity.Department;
import com.situ.entity.Doctor;
import com.situ.entity.PageEntity;
import com.situ.entity.Role;
import com.situ.entity.User;
import com.situ.service.IDepartmentService;
import com.situ.service.IDoctorService;
import com.situ.service.impl.DepartmentServiceImpl;
import com.situ.service.impl.DoctorServiceImpl;
import com.situ.util.BaseServlet;

/**
 * Servlet implementation class DoctorServlet
 */
@WebServlet("/doctor")
public class DoctorServlet extends BaseServlet {
	private static final long serialVersionUID = 1L;
       IDoctorService iDoctorService = new DoctorServiceImpl();
       IDepartmentService departmentService = new DepartmentServiceImpl();
	public void list( Doctor doctor ,PageEntity page, HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {
		List<Doctor> list = iDoctorService.selectlist(doctor.getName(),doctor.getCardno(),doctor.getDeptId(),page);
		    Integer sum =  iDoctorService.countDept(doctor.getName(),doctor.getCardno(),doctor.getDeptId());
		    request.setAttribute("pageNum", page.getPageNum());
			request.setAttribute("pageSize", page.getPageSize());
			request.setAttribute("doctors", list);
			request.setAttribute("sum", sum);
			request.setAttribute("abc", "abc");
			request.getRequestDispatcher("/hospital/doctorlist.jsp").forward(request, response);
	}
   
	public void  addskip(HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {//把科室的信息传过去
		
		List<Department> depts = departmentService.selectdeptAll();

		request.setAttribute("depts", depts);

		request.getRequestDispatcher("/hospital/doctoradd.jsp").forward(request, response);
		
	}
	public void update(@Param("id") Integer id,HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {
		// TODO Auto-generated method stub

//				
				Doctor doctor = iDoctorService.selectByid(id);
				request.setAttribute("doctor", doctor);
//				
				List<Department> depts = departmentService.selectdeptAll();

				request.setAttribute("depts", depts);

				request.getRequestDispatcher("/hospital/doctoradd.jsp").forward(request, response);
	}
	public void add(Doctor doctor,HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {
		if (doctor.getId()!= null) {
			iDoctorService.update(doctor);
		}else {
			iDoctorService.add(doctor);
		}
		request.getRequestDispatcher("/hospital/doctorlist.jsp").forward(request, response);
	}
	public void del(@Param("id")Integer id,HttpServletRequest request, HttpServletResponse response) {
		iDoctorService.deletDoctor(id);
		
	}
	 public void  selectDoctorAll(HttpServletRequest request, HttpServletResponse response ) throws IOException{

			response.setCharacterEncoding("UTF-8");
			
			response.setContentType("application/json; charset=UTF-8");
			//返回json
			List<Doctor> list = iDoctorService.selectDoctorAll();
			// 然后用工具类把它转为json类型
			String listStr = JSON.toJSONString(list);
			response.getWriter().write(listStr);
   
	 }
	 
}
