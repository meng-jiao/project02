package com.situ.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.situ.entity.Menu;
import com.situ.service.IMenuService;
import com.situ.service.impl.MenuServiceImpl;
@WebServlet("/menu/add")
public class MenuServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1555732953001328555L;
	 static IMenuService IMenuService = new MenuServiceImpl();
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//获取参数
		String name = request.getParameter("name");
		String url = request.getParameter("url");
		String parentId = request.getParameter("parentId");
		String sequence = request.getParameter("sequence");
		Menu menu = new Menu();
		menu.setName(name);
		menu.setUrl(url);
		menu.setParentId(Integer.parseInt(parentId));
		menu.setSequence(Integer.parseInt(sequence));
		//创建了个实体类对象并且，把参数对应赋值给他
		//调用方法存入就可
		 int i = IMenuService.reg(menu);
		 response.setContentType("text/html;charset=UTF-8");
		if (i==1) {
			
			response.getWriter().write("成功！");
		}else {
			response.getWriter().write("失败！");
		}
	
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		super.doPost(request, response);
	}

}
