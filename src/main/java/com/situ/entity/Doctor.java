package com.situ.entity;

import java.io.Serializable;

import lombok.Data;
@Data
public class Doctor implements Serializable {

	/**
	 * 
	 */
	
	private static final long serialVersionUID = 1260618995636275400L;

	private Integer id;
	private String name;//姓名
	private String cardno;//工作证号
	private String title;//职称
	private Integer age;//年龄
	private String gender;//性别
	private Integer deptId;//科室
	 private String deptName;//科室名
}
