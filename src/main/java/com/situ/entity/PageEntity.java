package com.situ.entity;

import lombok.Data;

@Data
public class PageEntity {

	private Integer pageSize;
	private Integer pageNum;
}
