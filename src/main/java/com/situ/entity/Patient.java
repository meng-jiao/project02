package com.situ.entity;

import java.io.Serializable;

import lombok.Data;

@Data
public class Patient implements Serializable {
	private static final long serialVersionUID = -2096254757292592831L;

	private Integer id;
	private String name;//姓名
	private String gender;
	private Integer age;
	private String inpatientno;//住院号
	private Integer doctorId;
	private Integer wardId;
	private String doctorName;//主管医生名
	private String wardName;//病房号名
}
