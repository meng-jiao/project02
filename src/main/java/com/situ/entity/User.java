package com.situ.entity;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

import lombok.Data;

@Data
public class User implements Serializable {

    private static final long serialVersionUID = 370488581862801030L;
    private Integer id;
    private String username;
    private String password;
    private String name;
    private Integer age;
    private String gender;
    private Date birthday;
    private Integer status;
    private String email;
    private Integer roleId;
    private String roleName;
    private Integer createby;
    private Timestamp createTime;
}
