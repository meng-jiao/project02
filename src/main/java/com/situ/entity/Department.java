package com.situ.entity;

import java.io.Serializable;

import lombok.Data;
@Data
public class Department implements Serializable {
//科室
	/**
	 * 
	 */
	private static final long serialVersionUID = -7011131024921203644L;
	private Integer id;
	private String name;//名称
	private String tel;//电话
	private String address;//地址
	
	
	
	
}
