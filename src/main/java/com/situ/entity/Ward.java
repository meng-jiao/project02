package com.situ.entity;

import java.io.Serializable;

import lombok.Data;

@Data
public class Ward implements Serializable {

	private static final long serialVersionUID = -6801545640354424780L;

	private Integer id;
	private String wardno;// 病房号
	private String address;// 地址
	private Integer deptId;// 科室

	private String deptName;// 科室名
}
