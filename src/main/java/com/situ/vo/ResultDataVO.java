package com.situ.vo;

import java.io.Serializable;
import java.util.List;

import lombok.Data;
//这个是要求返回格式规范的类，返回的必须是这个种格式
@Data
public class ResultDataVO implements Serializable {


	private static final long serialVersionUID = 5328776638076380000L;
     
	
	private int code;//返回的状态
	
	private String msg;//传一个信息
	
	private long count;//一共多少条
	
	private List<?> data;//封装的一条一条的数据
	
}
