package com.situ.dao;

import java.sql.SQLException;
import java.util.List;


import com.situ.entity.Patient;
import com.situ.util.DataSourceUtil;
import com.situ.util.StringUtil;

public class PatientDao {

	public static List<Patient> selectlist(String name, String inpatientno, Integer doctorId, Integer wardId, Integer a,
			Integer b) {
		// TODO Auto-generated method stub

		// TODO Auto-generated method stub
		String sql = "select patient.* , doctor.`name` AS 'doctor_name' , ward.wardno AS 'ward_name' from patient left join doctor on patient.doctor_id = doctor.id  " + 
				"  LEFT JOIN  ward ON  ward.id = patient.ward_id ";
		if (!StringUtil.isEmpty(name) || !StringUtil.isEmpty(inpatientno)||!StringUtil.isEmpty(doctorId)||!StringUtil.isEmpty(wardId)) {
			 sql += " where ";
		}
		if (!StringUtil.isEmpty(name)) {
			
			sql += " instr(patient.name,'" + name + "')>0 ";
		}
		if (!StringUtil.isEmpty(inpatientno)) {
			if (!StringUtil.isEmpty(name)) {
				sql += " and ";
			}
			sql += " instr(patient.inpatientno,'" + inpatientno + "')>0 ";
		}
		if (!StringUtil.isEmpty(doctorId)) {
			if (!StringUtil.isEmpty(name) || !StringUtil.isEmpty(inpatientno) ) {
				sql += " and ";
			}
			sql += " patient.doctor_id= " + doctorId + " ";
		}
		if (!StringUtil.isEmpty(wardId)) {
			if (!StringUtil.isEmpty(name) || !StringUtil.isEmpty(inpatientno)||!StringUtil.isEmpty(doctorId) ) {
				sql += " and ";
			}
			sql += " patient.ward_id=" + wardId + " ";
		}
		sql += " limit ?,? ";
		try {
			return DataSourceUtil.executeQuery(sql,Patient.class, a,b);
		} catch (InstantiationException | IllegalAccessException | NoSuchFieldException | SecurityException
				| SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
	
		
	}

	public static Integer countPatient(String name, String inpatientno, Integer doctorId, Integer wardId) {
		// TODO Auto-generated method stub
		String sql = "select count(id) from patient  ";
		if (!StringUtil.isEmpty(name) || !StringUtil.isEmpty(inpatientno)||!StringUtil.isEmpty(doctorId)||!StringUtil.isEmpty(wardId)) {
			 sql += " where ";
		}
		if (!StringUtil.isEmpty(name)) {
			
			sql += " instr(patient.name,'" + name + "')>0 ";
		}
		if (!StringUtil.isEmpty(inpatientno)) {
			if (!StringUtil.isEmpty(name)) {
				sql += " and ";
			}
			sql += " instr(patient.inpatientno,'" + inpatientno + "')>0 ";
		}
		if (!StringUtil.isEmpty(doctorId)) {
			if (!StringUtil.isEmpty(name) || !StringUtil.isEmpty(inpatientno) ) {
				sql += " and ";
			}
			sql += " patient.doctor_id=" + doctorId + " ";
		}
		if (!StringUtil.isEmpty(wardId)) {
			if (!StringUtil.isEmpty(name) || !StringUtil.isEmpty(inpatientno)||!StringUtil.isEmpty(doctorId) ) {
				sql += " and ";
			}
			sql += " patient.ward_id=" + wardId + " ";
		}
		try {
		 return	DataSourceUtil.executeCount(sql);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public static List<Patient> selectById(Integer id) {
		String sql = "select * from patient where id=? ";
		
		try {
			return  DataSourceUtil.executeQuery(sql, Patient.class, id);
		} catch (InstantiationException | IllegalAccessException | NoSuchFieldException | SecurityException
				| SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public static void update(Patient patient) {
		// TODO Auto-generated method stub
		String sql = "update patient set name=?, gender=?,age=?,inpatientno=?,doctor_id=?,ward_id=?  where id=?";
		try {
			DataSourceUtil.executeUpdate(sql, patient.getName(),patient.getGender(),patient.getAge(),patient.getInpatientno(),patient.getDoctorId(),patient.getWardId(),patient.getId());
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void add(Patient patient) {
		// TODO Auto-generated method stub
		String sql = "insert into patient(name,gender,age,inpatientno,doctor_id,ward_id) values(?,?,?,?,?,?)";
		try {
			DataSourceUtil.executeUpdate(sql,patient.getName(),patient.getGender(),patient.getAge(),patient.getInpatientno(),patient.getDoctorId(),patient.getWardId());
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void deletPatient(Integer id) {
		// TODO Auto-generated method stub
		String sql = "delete from  patient where  id = ? ";
		try {
			DataSourceUtil.executeUpdate(sql, id);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
