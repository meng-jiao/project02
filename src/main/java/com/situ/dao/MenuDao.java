package com.situ.dao;

import java.sql.SQLException;

import com.situ.entity.Menu;
import com.situ.util.DataSourceUtil;




public class MenuDao {
	/**
	 * 添加菜单
	 * @param menu
	 * @return
	 */
	public int reg(Menu menu) {
		String sql = "insert into menu  (name, url, parent_id,sequence) values(?,?,?,?)";
		try {
			return DataSourceUtil.executeUpdate(sql, menu.getName(), menu.getUrl(), menu.getParentId(),
					menu.getSequence());
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return 0;

	}
	
}
