package com.situ.dao;

import java.sql.SQLException;
import java.util.List;

import com.situ.entity.Doctor;

import com.situ.util.DataSourceUtil;
import com.situ.util.StringUtil;

public class DoctorDao {

	public static List<Doctor> selectlist(String name, String cardno, Integer deptId, Integer a, Integer b) {
		// TODO Auto-generated method stub
		String sql = "select doctor.*,department.name 'dept_name' from doctor left join department on department.id=doctor.dept_id  ";
		
		if (!StringUtil.isEmpty(name) || !StringUtil.isEmpty(cardno) || !StringUtil.isEmpty(deptId)) {
			sql += " where ";
		}

		if (!StringUtil.isEmpty(name)) {
			sql += " instr(doctor.name,'" + name + "')>0 ";
		}
		if (!StringUtil.isEmpty(cardno)) {
			if (!StringUtil.isEmpty(name)) {
				sql += " and ";
			}
			sql += " instr(doctor.cardno,'"+ cardno +"')>0 ";
		}
		if (!StringUtil.isEmpty(deptId)) {
			if (!StringUtil.isEmpty(name) || !StringUtil.isEmpty(cardno)) {
				sql += " and ";
			}
			sql += " doctor.dept_id=" + deptId + " ";
		}
		sql += " limit ?,?";
		try {
			return DataSourceUtil.executeQuery(sql, Doctor.class, a, b);

		} catch (InstantiationException | IllegalAccessException | NoSuchFieldException | SecurityException
				| SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public static Integer countDept(String name, String cardno, Integer deptId) {
		String sql = "select count(id) from doctor ";
		if (!StringUtil.isEmpty(name) || !StringUtil.isEmpty(cardno) || !StringUtil.isEmpty(deptId)) {
			sql += " where ";
		}

		if (!StringUtil.isEmpty(name)) {

			sql += " instr(name,'" + name + "')>0 ";
		}
		if (!StringUtil.isEmpty(cardno)) {
			if (!StringUtil.isEmpty(name)) {
				sql += " and ";
			}
			sql += " instr(doctor.cardno,'" + cardno + "')>0 ";
		}
		if (!StringUtil.isEmpty(deptId)) {
			if (!StringUtil.isEmpty(name) || !StringUtil.isEmpty(cardno)) {
				sql += " and ";
			}
			sql += " doctor.dept_id=" + deptId + " ";
		}
		try {
			return DataSourceUtil.executeCount(sql);

		} catch (SecurityException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return 0;

	}

	public static void update(Doctor doctor) {
		// TODO Auto-generated method stub
		String sql = "update doctor set name=?, cardno=?,title=?,age=?,gender=?,dept_id=?  where id=?";
		try {
			DataSourceUtil.executeUpdate(sql,doctor.getName(),doctor.getCardno(),doctor.getTitle(),doctor.getAge(),doctor.getGender(),doctor.getDeptId(),doctor.getId() );
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void add(Doctor doctor) {
		
		String sql = "insert into doctor(name,cardno,title,age,gender,dept_id) values(?,?,?,?,?,?)";
		try {
			DataSourceUtil.executeUpdate(sql,doctor.getName(),doctor.getCardno(),doctor.getTitle(),doctor.getAge(),doctor.getGender(),doctor.getDeptId());
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
	}

	public static List<Doctor> selectByid(Integer id) {
		// TODO Auto-generated method stub
		String sql = "select * from doctor where id=? ";
		try {
		 return	DataSourceUtil.executeQuery(sql, Doctor.class, id);
		} catch (InstantiationException | IllegalAccessException | NoSuchFieldException | SecurityException
				| SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public static void deletDoctor(Integer id) {
		// TODO Auto-generated method stub
		String sql = "delete from  doctor where  id = ? ";
		try {
			DataSourceUtil.executeUpdate(sql, id);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static List<Doctor> selectDoctorAll() {
		// TODO Auto-generated method stub
		String sql = "select * from doctor ";
		try {
		  return	DataSourceUtil.executeQuery(sql, Doctor.class);
		} catch (InstantiationException | IllegalAccessException | NoSuchFieldException | SecurityException
				| SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

}
