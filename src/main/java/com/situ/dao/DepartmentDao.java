package com.situ.dao;

import java.sql.SQLException;
import java.util.List;

import com.situ.entity.Department;
import com.situ.util.DataSourceUtil;
import com.situ.util.StringUtil;

public class DepartmentDao {

	public static List<Department> selectUser(String name, Integer a, Integer b) {
		// TODO Auto-generated method stub
		String sql = "select * from department  ";
		if (!StringUtil.isEmpty(name)) {
			 sql += " where ";
		}
		if (!StringUtil.isEmpty(name)) {
			
			sql += " instr(department.name,'" + name + "')>0 ";
		}
		sql += " limit ?,? ";
		try {
			return DataSourceUtil.executeQuery(sql,Department.class, a,b);
		} catch (InstantiationException | IllegalAccessException | NoSuchFieldException | SecurityException
				| SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
	}

	public static Integer countDept(String name) {
		// TODO Auto-generated method stub
		String sql = "select count(id) from department ";
		if (!StringUtil.isEmpty(name)) {
			 sql += " where ";
		}
		
		
		if (!StringUtil.isEmpty(name)) {
			
			sql += " instr(name,'" + name + "')>0 ";
		}
		
		try {
			return DataSourceUtil.executeCount(sql);

		} catch (SecurityException
				| SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return 0;
		
		
	
	}

	public static void update(Department dept) {
		// TODO Auto-generated method stub
		String sql = "update department set name=?, tel=?,address=?  where id=?";
		try {
			DataSourceUtil.executeUpdate(sql, dept.getName(),dept.getTel(),dept.getAddress(),dept.getId());
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void add(Department dept) {
		// TODO Auto-generated method stub
		String sql = "insert into department(name,tel,address) values(?,?,?)";
		try {
			DataSourceUtil.executeUpdate(sql, dept.getName(),dept.getTel(),dept.getAddress());
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static List<Department> seleDept(Integer id) {
		// TODO Auto-generated method stub
		String sql = "select * from department where id=? ";
		try {
			return DataSourceUtil.executeQuery(sql,Department.class,id);
		} catch (InstantiationException | IllegalAccessException | NoSuchFieldException | SecurityException
				| SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public static void deletDepat(Integer id) {
		// TODO Auto-generated method stub
		String sql = "delete from  department where  id = ? ";
		try {
			DataSourceUtil.executeUpdate(sql, id);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static List<Department> selectdeptAll() {
		String sql = "select * from department  ";
		try {
			return DataSourceUtil.executeQuery(sql,Department.class);
		} catch (InstantiationException | IllegalAccessException | NoSuchFieldException | SecurityException
				| SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

}
