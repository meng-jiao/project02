package com.situ.dao;

import java.sql.SQLException;
import java.util.List;

import com.situ.entity.Doctor;
import com.situ.entity.Ward;
import com.situ.util.DataSourceUtil;
import com.situ.util.StringUtil;

public class WardDao {

	public static List<Ward> selectlist(String wardno, Integer deptId, Integer a, Integer b) {
		// TODO Auto-generated method stub
		String sql = "select ward.*,department.name 'dept_name' from ward left join department on department.id=ward.dept_id  ";
		
		if (!StringUtil.isEmpty(wardno) || !StringUtil.isEmpty(deptId) ) {
			sql += " where ";
		}

		if (!StringUtil.isEmpty(wardno)) {
			sql += " instr(ward.wardno,'" + wardno + "')>0 ";
		}
		if (!StringUtil.isEmpty(deptId)) {
			if (!StringUtil.isEmpty(wardno)) {
				sql += " and ";
			}
			sql += "  ward.dept_id ="+ deptId +" ";
		}
		
		sql += " limit ?,?";
		try {
			return DataSourceUtil.executeQuery(sql, Ward.class, a, b);

		} catch (InstantiationException | IllegalAccessException | NoSuchFieldException | SecurityException
				| SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public static Integer countWard(String wardno, Integer deptId) {
		String sql = "select count(id) from ward ";
		if (!StringUtil.isEmpty(wardno) || !StringUtil.isEmpty(deptId) ) {
			sql += " where ";
		}

		if (!StringUtil.isEmpty(wardno)) {
			sql += " instr(ward.wardno,'" + wardno + "')>0 ";
		}
		if (!StringUtil.isEmpty(deptId)) {
			if (!StringUtil.isEmpty(wardno)) {
				sql += " and ";
			}
			sql += "  ward.dept_id ="+ deptId +" ";
		}
		try {
			return DataSourceUtil.executeCount(sql);

		} catch (SecurityException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return 0;

	}

	public static void update(Ward ward) {
		// TODO Auto-generated method stub
		String sql = "update ward set wardno=?, address=?,dept_id=? where id=?";
		try {
			DataSourceUtil.executeUpdate(sql,ward.getWardno(), ward.getAddress(),ward.getDeptId(),ward.getId());
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void add(Ward ward) {
		
		String sql = "insert into ward(wardno,address,dept_id) values(?,?,?)";
		try {
			DataSourceUtil.executeUpdate(sql,ward.getWardno(),ward.getAddress(),ward.getDeptId());
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
	}

	public static List<Ward> selectByid(Integer id) {
		// TODO Auto-generated method stub
		String sql = "select * from ward where id=? ";
		try {
			return DataSourceUtil.executeQuery(sql, Ward.class,id);
		} catch (InstantiationException | IllegalAccessException | NoSuchFieldException | SecurityException
				| SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public static void deletWard(Integer id) {
		// TODO Auto-generated method stub
		String sql = "delete from  ward where  id = ? ";
		try {
			DataSourceUtil.executeUpdate(sql, id);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static List<Ward> selectWardAll() {
		String sql = "select * from  ward  ";
		
		try {
			return DataSourceUtil.executeQuery(sql, Ward.class);
		} catch (InstantiationException | IllegalAccessException | NoSuchFieldException | SecurityException
				| SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

}
