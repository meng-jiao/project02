package com.situ.dao;

import java.sql.SQLException;
import java.util.List;

import com.situ.entity.User;
import com.situ.util.DataSourceUtil;
import com.situ.util.StringUtil;

public class UserDao {

	public int addUser(User user) {
		String sql = "insert into user(username,password,gender,email,birthday,age,name,role_id,createby,create_time) values(?,?,?,?,?,?,?,?,?,?)";
		try {
			return DataSourceUtil.executeUpdate(sql, user.getUsername(), user.getPassword(), user.getGender(),
					user.getEmail(), user.getBirthday(), user.getAge(), user.getName(),user.getRoleId(),user.getCreateby(),user.getCreateTime());
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return 0;
	}

	public List<User> check(String username, String password) {
		// TODO Auto-generated method stub
		String sql = "select* from user where username=? and password=?";
		try {
			return DataSourceUtil.executeQuery(sql, User.class, username, password);

		} catch (InstantiationException | IllegalAccessException | NoSuchFieldException | SecurityException
				| SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	
	public Integer countUsers(String username, String name ) throws InstantiationException, NoSuchFieldException, IllegalAccessException {
		String sql = "select count(id) from user ";
		if (!StringUtil.isEmpty(username)|| !StringUtil.isEmpty(name)) {
			 sql += " where ";
		}
		
		if (!StringUtil.isEmpty(username)) {
			sql += " instr(username,'" + username + "')>0";
		}
		if (!StringUtil.isEmpty(name)) {
			if (!StringUtil.isEmpty(username)) {
				sql += " and ";
			}
			sql += " instr(name,'" + name + "')>0 ";
		}
		
		try {
			return DataSourceUtil.executeCount(sql);

		} catch (SecurityException
				| SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return 0;
	}

	
	public List<User> selectUser(String username, String name,Integer a,Integer b) {
		String sql = "select user.*,role.name 'role_name' from user left join role on role.id=user.role_id  ";
				
		if (!StringUtil.isEmpty(username)|| !StringUtil.isEmpty(name)) {
			 sql += " where ";
		}
		
		if (!StringUtil.isEmpty(username)) {
			sql += " instr(username,'" + username + "')>0";
		}
		if (!StringUtil.isEmpty(name)) {
			if (!StringUtil.isEmpty(username)) {
				sql += " and ";
			}
			sql += " instr(user.name,'" + name + "')>0 ";
		}
		sql+=" limit ?,?";
		try {
			return DataSourceUtil.executeQuery(sql, User.class,a,b);

		} catch (InstantiationException | IllegalAccessException | NoSuchFieldException | SecurityException
				| SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public List<User> selectAll(Integer a, Integer b) {//用不到了
		String sql = "select * from user limit ?,?";
		try {
			return DataSourceUtil.executeQuery(sql, User.class,a,b);

		} catch (InstantiationException | IllegalAccessException | NoSuchFieldException | SecurityException
				| SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public void userIdDele(Integer id) {
		// TODO Auto-generated method stub
		String sql = "delete from  user where  id = ? ";
		try {
			DataSourceUtil.executeUpdate(sql, id);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public List<User> selectByid(String id) {
		// TODO Auto-generated method stub
		String sql = "select* from user where id =?";
		try {
			return DataSourceUtil.executeQuery(sql, User.class, Integer.parseInt(id));
		} catch (NumberFormatException | InstantiationException | IllegalAccessException | NoSuchFieldException
				| SecurityException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;

	}

	public void updateUser(User user) {
		// TODO Auto-generated method stub
		String sql = "update user set username=?, password=?,name=?,gender=?,age=?,birthday=?,email=?,role_id=? where id=?";
		try {
			DataSourceUtil.executeUpdate(sql, user.getUsername(), user.getPassword(), user.getName(), user.getGender(),
					user.getAge(), user.getBirthday(), user.getEmail(),user.getRoleId(), user.getId());
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	
}
