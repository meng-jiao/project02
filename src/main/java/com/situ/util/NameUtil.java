package com.situ.util;

public class NameUtil {

	/**
	 * 下划线命名 转 驼峰命名
	 * 
	 * @param _name 下划线命名
	 * @return 驼峰命名
	 */
	public static String underlineToHump(String _name) {
		StringBuilder sb = new StringBuilder();
		if (null != _name && _name.length() > 0) {
			String[] strs = _name.split("_");
			for (int i = 0; i < strs.length; i++) {
				if (i==0) {
					sb.append(strs[i].toLowerCase());
				} else {
					sb.append(strs[i].substring(0, 1).toUpperCase());
					sb.append(strs[i].substring(1).toLowerCase());
				}
			}
		}
		return sb.toString();
	}

}
