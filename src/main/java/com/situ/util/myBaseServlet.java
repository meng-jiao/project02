package com.situ.util;

import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.Enumeration;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.situ.annotation.Param;

public class myBaseServlet extends HttpServlet {

	private static final long serialVersionUID = -3643747821372738799L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// TODO Auto-generated method stub
		req.setCharacterEncoding("utf-8");
		String action = req.getParameter("action");
		if (action == null) {
			action = "list";
		}
		// 获取类的所有方法
		//
		// c1.getDeclaredMethods();
		//
		// Method[] methods = c1.getDeclaredMethods(); Method 方法类型

		Method[] methods = this.getClass().getDeclaredMethods();// 获取该类的所以方法
		Method method = null;
		for (Method m : methods) {
			if (m.getName().equals(action)) {//// getName 返回由Method对象表示的方法名称，以字符串形式返回。
				method = m;
				break;
			}
		}
		// 使用了method.setAccessible(true)后 性能有了20倍的提升；
		// Accessable属性是继承自AccessibleObject 类. 功能是启用或禁用安全检查 ；
		method.setAccessible(true);
		invokeMethod(method, req, resp);

	}

	private void invokeMethod(Method method, HttpServletRequest req, HttpServletResponse resp) {
		// TODO Auto-generated method stub
		Parameter[] ps = method.getParameters();// 获取这个方法的所有参数
		Class<?>[] cs = method.getParameterTypes();// 获取所有参数对应的类型
		Object[] args = new Object[ps.length];
		for (int i = 0; i < ps.length; i++) {
			String paramName = "";
			Param ann = ps[i].getAnnotation(Param.class);// 方法返回该元素的指定类型的注释，
			if (ann != null) {
				paramName = ann.value();
			}
			if (cs[i].equals(int.class)) {
				args[i] = 0;
			} else if (cs[i].equals(double.class)) {
				args[i] = 0;
			}

			
			
			
			
			
			if (cs[i].equals(HttpServletRequest.class)) {
				args[i] = req;
			} else if (cs[i].equals(HttpServletResponse.class)) {
				args[i] = resp;
			} else if (cs[i].equals(HttpSession.class)) {
				args[i] = req.getSession();
				//前三个是固定的request   response   session
			} else if (cs[i].equals(Integer.class) || cs[i].equals(int.class)) {
				String arg = req.getParameter(paramName);//把request里面的参数拿出来，
				if (arg != null) {
					args[i] = Integer.parseInt(arg);
				}
			} else if (cs[i].equals(double.class) || cs[i].equals(Double.class)) {
				String arg = req.getParameter(paramName);
				if (arg != null) {
					args[i] = Double.parseDouble(arg);
				}
			} else if (cs[i].equals(String.class)) {//拿出integer，string，double都拿出
				args[i] = req.getParameter(paramName);
			} else {//前面的都拿完后剩下的就是实体类了
				try {
					// newInstance()实际上是把new这个方式分解为两步，即首先调用Class加载方法加载某个类，然后实例化。
					
					Object obj = cs[i].newInstance();// newInstance()只能调用无参构造方法,实例化对象
					//反射动态创建实体类
					Enumeration<String> names = req.getParameterNames();
					//获取前台传来的所有方法名
					// request.getParameterNames()方法是将发送请求页面中form表单里所有具有name属性的表单对象获取(包括button).
					// 返回一个Enumeration类型的枚举.

					while (names.hasMoreElements()) {// hasMoreElements() 是判断是不是还要下一个元素，
						String name = names.nextElement();// 连续调用 nextElement 方法将返回一系列的连续元素。
						// 如果Enumeration对象还有元素，该方法得到下一个元素
						try {
							// Field是一个类,位于java.lang.reflect包下。在Java反射中Field类描述的是类的属性信息，功能包括：

							// 获取当前对象的成员变量的类型
							// 对成员变量重新设值

							// getDeclaredField(String name): 获取类特定的方法，name参数指定了属性的名称
							// Field.getType()：返回这个变量的类型
							// Field.getGenericType()：如果当前属性有签名属性类型就返回，否则就返回 Field.getType()
							// isEnumConstant() ： 判断这个属性是否是枚举类

							Field field = cs[i].getDeclaredField(name);
							field.setAccessible(true);
							String param = req.getParameter(name);
							// 获取和修改成员变量的值
							//
							// getName() ： 获取属性的名字
							//
							// get(Object obj) 返回指定对象obj上此 Field 表示的字段的值
							//
							// set(Object obj, Object value) 将指定对象变量上此 Field 对象表示的字段设置为指定的新值

							if (field.getType().equals(Integer.class)) {
								field.set(obj, Integer.parseInt(param));
							} else if (field.getType().equals(Double.class)) {
								field.set(obj, Double.parseDouble(param));
							} else {
								field.set(obj, param);

							}
						} catch (Exception ignore) {

						}
					}
					args[i] = obj;
				} catch (Exception ignore) {
				}
			}
		}
		try {
			//invoke是去执行那个方法。。参数放方法类型，加参数   args是这个参数的整合体
			method.invoke(this, args);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(req, resp);
	}

}
